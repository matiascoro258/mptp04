import os

def continuar():
    print()
    input('presione una tecla para continuar ...')
    os.system('cls')

def menu():
    print('1) Registrar productos')
    print('2) Mostrar el listado de Productos.')
    print('3) Mostrar stock desde/hasta')
    print('4) Sumar x al stock')
    print('5) Eliminar stock ')
    print('6) Salir')
    eleccion = int(input('Elija una opción: '))
    while not((eleccion >= 1) and (eleccion <= 7)):
        eleccion = int(input('Elija una opción: '))
    os.system('cls')
    return eleccion


def precio1():
    precio=input('precio')
    while not(precio.isdigit()):
        print('Valor incorrecto')
        precio=input('precio')
    precio=float(precio)
    return precio

def stock1():
    stock=input('cantidad')
    while not(stock.isdigit()):
        print('Valor incorrecto')
        stock=input('cantidad')
    stock=int(stock)
    return stock

def validadCodigo():
    while True:
        try:
            entrada=int(input('Ingrese codigo'))
            break
        except ValueError:
            print('Ingrese el codigo correctamente')
    return entrada


def leerProductos():
    print('Cargar Lista de Productos')
    producto={}
    continuar='s'
    while(continuar=='s'):
        codigo =validadCodigo()
        if codigo not in producto:
            descripcion=input('Descripcion')
            precio=precio1()
            stock=stock1()
            producto[codigo] = [descripcion,precio,stock]
            print('Agregado correctamente')
        else:
            print('El codigo ya existe')
        continuar=input('Continuar cargando productos? s/n: ')
    return producto


def mostrar(diccionario):
    print('Listado de Productos')
    for clave, valor in diccionario.items():
        print(clave,valor)


def leerhasta(desde):
    hasta=int(input("Ingrese stock hasta:"))
    while not(hasta>=desde):
        hasta=int(input('Ingrese stock hasta'))
    return hasta

def mostrar_stock(producto):
    desde=int(input("Ingrese stock desde:"))
    hasta=leerhasta(desde)
    for clave, valor in producto.items():
        if(producto[clave][2]>=desde) and (producto[clave][2]<=hasta):
            print(clave,valor)

def sumar_stock(producto):
    Y=int(input("Ingrese Y:"))
    x=int(input('Ingrese x'))
    for clave, valor in producto.items():
        if(valor[2]<=Y and valor[2]>0):
            valor[2]=(valor[2]+x)   
            print(clave,valor)

def eliminar(producto):
    print('Elimina los productos con stok = 0')
    i=1
    while i<=len(producto):
        for clave,valor in producto.items():
            if(valor[2]==0):
                print('Se va a eliminar:')
                del producto[clave]
                print('Eliminado.....')
                break
            i+=1
    return(producto)


def salir():
    print('Fin del programa...')


#principal
opcion = 0

os.system('cls')
while (opcion != 7):
    opcion = menu()
    if opcion == 1:
        estudiantes = leerProductos()
    elif opcion == 2:
        mostrar(estudiantes)
    elif opcion == 3:
        mostrar_stock(estudiantes)
    elif opcion == 4:
        sumar_stock(estudiantes)
    elif opcion == 5:
        eliminar(estudiantes)    
    elif (opcion == 6):        
        salir()
    continuar()